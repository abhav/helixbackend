import sys
import include
import requests
import json
from . import UserConstants
sys.path.append('../')


def genCust():
    for agent in UserConstants.agentArray:
        req = requests.post(url=include.signupURL, data=agent)
        resp = json.loads(req.text)
        if UserConstants.SUCCESS in resp:
            print('Create user %s successful.' % agent[UserConstants.PHONE])
        else:
            print("Error code %s: %s" % (resp['Error'], resp['Message']) )
    return 1

genCust()
from . import constants
from flask import Flask  # import flask
from flask import request
from . import db
import json
from flask import make_response
from . import util

app = Flask(__name__)  # create an app instance
Debug = True
ActiveTokens = {'token': 'token'}
providerWorkHours = {}
useMySQL = True
userTokens = {}


def return_json(arg):
    resp = make_response(json.dumps(arg, sort_keys=True, indent=4))
    resp.headers['Content-type'] = "application/json"
    return resp


def insertUserToken(userid, token):
    global userTokens
    userTokens[userid] = token
    return 1


def getUserToken(userid):
    global userTokens
    if userTokens is None:
        return None
    if userTokens == {}:
        return None
    if not userid in userTokens:
        return None
    return userTokens[userid]


def createSuperUser():
    print('In auth createSuperUser.')
    user_details = {}
    user_name = 'root'
    primary_key = '7165782343'
    user_email = "sampletest@housecuts.live"
    password = "GoHouseCuts"
    dateofbirth = '01/01/1990'

    user_details[constants.PHONE] = primary_key
    # See if the primary_key exists in the database
    if db.doesUserExistInDB(primary_key):
        print('Superuser exists.')
        return

    user_details[constants.UTYPE] = constants.ADMIN
    user_details[constants.EMAIL] = user_email
    user_details[constants.FIRSTNAME] = user_name
    user_details[constants.LASTNAME] = user_name
    user_details[constants.PASSWORD] = password
    user_details[constants.USERSTATUS] = constants.ACTIVE
    user_details[constants.DATEOFBIRTH] = dateofbirth
    user_details[constants.GENDER] = 'Male'
    user_details[constants.PHOTO] = -1

    if Debug:
        print('Primary key: ', primary_key)
        print(user_details)

    # Insert user in the database
    db.insertUserInDB(primary_key, user_details)

    res = {constants.SUCCESS: 1, constants.UTYPE: constants.ADMIN}
    return res


@app.route("/")  # at the end point /
def hello():  # call method hello
    res = createSuperUser()
    return res  # which returns "hello world"


@app.route('/signup', methods=['GET', 'POST'])
def signUserOn():
    user_details = {}

    # Fetch All the values
    for elt in constants.userProfileProps:
        if request.values.get(elt, None) is None:
            res = {constants.ERROR: -3004, constants.MSG: "Signup Failed:  The required input %s is missing." % elt}
            return return_json(res)
        else:
            user_details[elt] = request.values.get(elt, None)

    typ = request.values.get(constants.UTYPE)
    if not int(typ) in constants.userTypes:
        res = {constants.ERROR: -3004,
               constants.MSG: "Signup Failed:  The identified user type %s is not /recongized." % typ}
        return return_json(res)

    pkey = user_details['PHONE']
    if db.doesUserExistInDB(pkey):
        res = {constants.ERROR: -3004,
               constants.MSG: "Signup Failed:  The user phone no is already registered"}
        return return_json(res)

    print(user_details)
    db.insertUserInDB(pkey, user_details)

    token = util.uniqueToken()

    insertUserToken(pkey, token)

    if Debug:
        print('Primary key: ', pkey)
        print('token: ', token)

    # Register the player document with the token
    ActiveTokens[token] = user_details[constants.constants.PHONE]

    res = {constants.SUCCESS: 1, constants.TOKEN: token}
    return res


@app.route('/login', methods=['GET', 'POST'])
def login():
    from flask import request
    """ req_data = request.values.get_json() """
    phone = request.values.get(constants.PHONE, None)
    user_passwd = request.values.get(constants.PASSWORD, None)

    if phone is None:
        res = {constants.ERROR: -3002, constants.MSG: "Login Failed:  %s is missing" % constants.PHONE}
        return return_json(res)
    if user_passwd is None:
        res = {constants.ERROR: -3002, constants.MSG: "Login Failed:  %s is missing" % constants.PASSWORD}
        return return_json(res)

    if db.doesUserExistInDB(phone):
        recordFound = db.authUserInDB(phone, user_passwd)
        print(recordFound)
        if recordFound is None:
            res = {constants.ERROR: -3002, constants.MSG: "Login Failed:  Invalid Password."}
            return return_json(res)
        else:
            # Register the player document with the token
            token = getUserToken(phone)

            if token is None:
                token = util.uniqueToken()

            ActiveTokens[token] = phone
            insertUserToken(phone, token)
            res = {constants.SUCCESS: 1, constants.TOKEN: token}
            return return_json(res)
    else:
        res = {constants.ERROR: -3001, constants.MSG: "Login Failed: Userid does not exist."}
        print(res)
        return return_json(res)


@app.route('/signout', methods=['GET', 'POST'])
def signout():
    from flask import request
    token = request.values.get(constants.TOKEN, None)
    if token is None:
        res = {constants.ERROR: -3020,
               constants.MSG: "getNameImage Failed:  Client failed to provide %s." % constants.TOKEN}
        return return_json(res)
    if token in ActiveTokens:
        del ActiveTokens[token]
    else:
        print("Error, provided token %s does not exist." % token)
    res = {"Success": 1}
    return return_json(res)


if __name__ == "__main__":  # on running python app.py
    # app.run()
    hello()

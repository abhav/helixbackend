TOKEN = "token"

PHONE = "phone"
FIRSTNAME = "fname"
LASTNAME = "lname"
EMAIL = "email"
PHOTO = "photo"
PASSWORD = "password"
DATEOFBIRTH = "dateofbirth"
GENDER = "gender"

UTYPE = "type"
ADMIN = 1
PATIENTS = 2
PHYSICIANS = 3

userTypes = [ADMIN, PATIENTS, PHYSICIANS]

USERSTATUS = "status"
ACTIVE = "active"
INACTIVE = "inactive"


SUCCESS = "Success"
ERROR = "Error"
MSG = "Message"

userProfileProps = [PASSWORD, PHONE, UTYPE, FIRSTNAME, LASTNAME, EMAIL, DATEOFBIRTH, GENDER, USERSTATUS]



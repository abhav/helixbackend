import os
import mysql.connector
from . import constants

hostname = None
username = None
password = None
database = None


def initialize():
    global hostname
    global username
    global password
    global database

    if hostname is None:
        # read the environmental variables and open a connection
        hostname = os.environ['hostname']
        username = os.environ['username']
        password = os.environ['password']
        database = 'Helix'
    return


def execDB(cmd):
    initialize()

    if True:
        print("In util.execDBdml with cmd %s" % cmd)
        print("hostname=%s, username=%s, password=%s, database=%s" % (hostname, username, password, database))

    myDB = None
    myConnection = None
    try:
        myConnection = mysql.connector.connect(host=hostname, user=username, passwd=password, db=database)
        myDB = myConnection.cursor()
        for c in cmd:
            print(c)
            myDB.execute(c)
        myConnection.commit()
        myDB.close()
        myConnection.close()
    except mysql.connector.Error as err:
        if not myDB is None:
            myDB.close()
        if not myConnection is None:
            myConnection.close()
        print("Error while executing of SQL %s failed." % cmd)
        print(format(err))
    return


def doesUserExistInDB(pkey):
    result = False
    qry = "select * from users where %s = '%s'" %(constants.PHONE,pkey)
    result = doesRowExist(qry)
    return result


def doesRowExist(qry):
    global hostname
    global username
    global password
    global database

    initialize()

    result = False

    if True:
        print("In util.doesRowExist with qry %s" % qry)
        print("hostname=%s, username=%s, password=%s, database=%s" % (hostname, username, password, database))

    myDB = None
    myConnection = None
    try:
        myConnection = mysql.connector.connect(host=hostname, user=username, passwd=password, db=database)
        myDB = myConnection.cursor()
        myDB.execute(qry)
        res = myDB.fetchall()

        if True:
            print("in doesRowExist number of rows={}".format(myDB.rowcount))
        if myDB.rowcount > 0:
            result = True
        myDB.close()
        myConnection.close()
    except mysql.connector.Error as err:
        if not myDB is None:
            myDB.close()
        if not myConnection is None:
            myConnection.close()
        print("Error in dbutil.doesRowExist: Execution of SQL %s failed." % qry)
        print(format(err))

    return result


def insertUserInDB(pkey, userdoc):
    attrL, valL = sqlDMLargs(userdoc, constants.userProfileProps)
    insertCMD = "insert into users (%s) values (%s);" % (attrL, valL)
    print(insertCMD)
    execDB(insertCMD)
    return


def authUserInDB(pkey, password):
    result = None
    qry = "select %s from users where %s='%s and %s = %s '" % (constants.PHONE, constants.PHONE, pkey,
                                                               constants.PASSWORD, password)
    result = doesRowExist(qry)
    return result


def sqlDMLargs (userdoc, tgtProps):
    attrL = ''
    valL = ''
    for k in userdoc.keys():
        if k in tgtProps:
            if len(attrL) > 0:
                attrL = attrL + ','
                valL = valL + ','
            attrL = attrL +  str(k)
            if isinstance(userdoc[k], str):
                val = fixStringToken(userdoc[k])
            else:
                val = userdoc[k]
            valL = valL + '\'' + str(val) + '\''
    return attrL, valL


def fixStringToken(tk):
    return tk.replace('\'', '\\\'')
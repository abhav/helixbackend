import random
import string


def randomString(stringLength=4):
    letters = string.ascii_lowercase
    rstr= ''.join(random.choice(letters) for i in range(stringLength))
    return '0X'+rstr


def uniqueToken(stringLength=4):
    global ActiveTokens
    done = False
    token = randomString()
    while (not done):
        if globals.ActiveTokens is None:
            return token
        if globals.ActiveTokens == {}:
            return token
        if not token in globals.ActiveTokens:
            return token
        token = randomString()
    return token